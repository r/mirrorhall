'''
The main Mirror Hall window
'''

import gi

gi.require_version('GstVideo', '1.0')
gi.require_version('Gst', '1.0')
gi.require_version('Gtk', '4.0')
from gi.repository import Adw, Gtk, GLib
from .mh_device_item import MhDeviceItem
from .libcast import MirrorNetworkService
from .libmirror import MirrorService
from .mh_player import GstMirrorWidget
from .mh_error_dialogs import MhErrorDialogs

@Gtk.Template(resource_path='/eu/nokun/MirrorHall/mh_window.ui')
class MirrorhallWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'MirrorhallWindow'
    sink_port = None
    set_prefer_dark_mode = None

    header_bar = Gtk.Template.Child()
    rtsp_banner = Gtk.Template.Child()
    label = Gtk.Template.Child()
    spinner = Gtk.Template.Child()
    switch_role = Gtk.Template.Child()
    server_ip_list = Gtk.Template.Child()
    client_view = Gtk.Template.Child()
    server_view = Gtk.Template.Child()
    outer_box = Gtk.Template.Child()
    main_view = Gtk.Template.Child()
    video_player = Gtk.Template.Child()
    listbox_devices = Gtk.Template.Child()
    mh_device_item = Gtk.Template.Child()
    toast_overlay = Gtk.Template.Child()
    toast = Adw.Toast(title="Tap the player to toggle fullscreen mode")

    def __init__(self, net_service: MirrorNetworkService, dbus_service: MirrorService, sink_port: int, **kwargs):
        super().__init__(**kwargs)
        self.net_service = net_service
        self.dbus_service = dbus_service
        self.res_limit = 1080
        self.sink_port = sink_port

        self.switch_role.connect("clicked", self.switch_role_cb)

        gesture = Gtk.GestureClick()
        gesture.connect("released", self.toggle_fullscreen)
        self.video_player.add_controller(gesture)

        self.set_device_list([])
        GLib.timeout_add_seconds(3, self._glib_update_device_list)

    # GLib task to refresh device list periodically
    def _glib_update_device_list(self):
        self.set_device_list(self.net_service.device_list.values())
        return True
    
    def set_rtsp_server_endpoint(self, endpoint):
        self.rtsp_banner.set_title("A virtual monitor is active on <a href='rtsp://{}:8554/{}'>rtsp://{}:8554/{}</a>.".format(self.net_service.get_iface_ips()[0], endpoint, self.net_service.get_iface_ips()[0], endpoint))
        self.rtsp_banner.set_revealed(endpoint is not None)

    # Binds the GStreamer player widget to the pipeline
    def setup_video_player(self):
        player = GstMirrorWidget(self.sink_port, self)
        while self.video_player.get_first_child():
            self.video_player.remove(self.video_player.get_first_child())
        self.video_player.append(player)

    # Toggles fullscreen mode
    def toggle_fullscreen(self, button, a = None, b = None, c = None):
        if self.is_fullscreen():
            self.server_ip_list.set_visible(True)
            self.main_view.add_css_class("padded-view")
            self.unfullscreen()
            self.header_bar.set_visible(True)
        else:
            self.server_ip_list.set_visible(False)
            self.main_view.remove_css_class("padded-view")
            self.fullscreen()
            self.header_bar.set_visible(False)
            
            if self.toast:
                self.toast.set_timeout(3)
                self.toast_overlay.add_toast(self.toast)
                self.toast = None

    # Sets the dark mode for the window
    def set_dark_mode(self, dark_mode):
            style_manager = Adw.StyleManager.get_default()
            if dark_mode:
                style_manager.set_color_scheme(Adw.ColorScheme.FORCE_DARK)
                self.outer_box.add_css_class("psychedelic-bg")
            else:
                style_manager.set_color_scheme(Adw.ColorScheme.DEFAULT)
                self.outer_box.remove_css_class("psychedelic-bg")

    def switch_role_cb(self, button):
        if self.main_view.get_visible_child_name() == "client_view":
            self.rtsp_banner.set_revealed(False)
            if not self.video_player.get_first_child():
                self.setup_video_player()
            self.net_service.mdns_start_announce()
            self.net_service.mdns_stop_discover()
            self.dbus_service.stop_all_streams()
            self.set_rtsp_server_endpoint(None)

            self.set_dark_mode(True)
            self.main_view.set_visible_child_name("server_view")
            self.switch_role.set_label("Exit the Mirror")
            self.server_ip_list.set_markup("🔴 live on <b>{}</b>, port <b>{}</b>".format(self.net_service.get_iface_ips()[0], self.sink_port))
        else:
            self.net_service.mdns_stop_announce()
            self.net_service.mdns_start_discover()

            self.set_dark_mode(False)
            self.main_view.set_visible_child_name("client_view")
            self.switch_role.set_label("Turn Into a Mirror")

        self.main_view.show()

    # Updates device list in UI
    def set_device_list(self, devices):
        self.clear_device_list()
        if devices is None or len(devices) == 0:
            self.spinner.show()
            self.listbox_devices.hide()
        else:
            self.spinner.hide()
            self.listbox_devices.show()

        for device in devices:
            item = MhDeviceItem(device)
            # TODO: bug when manual device is enabled by toggling other devices
            item.set_active(self.dbus_service.is_streaming(device))
            item.connect("clicked", self.toggle_stream)
            self.listbox_devices.append(item)

    # TODO: continuous clear+set is weird and glitchy
    def clear_device_list(self):
        while self.listbox_devices.get_first_child():
            self.listbox_devices.remove(self.listbox_devices.get_first_child())

    # Starts or stops streaming to given device
    def toggle_stream(self, item):
        device = item.device
        print("Turned stream to {} {}".format(device.name, "on" if item.get_active() else "off"))

        if item.get_active():
            try: 
                for res_current in device.resolutions:
                    res = [int(x) for x in res_current.split("x")]
                    if res[0] <= self.res_limit or res[1] <= self.res_limit:
                        break
                    # will stop at the lowest available otherwise
            except:
                print("Error parsing resolution, using 720p fallback")
                res = [1280, 720]
            print("Connecting to host {} ({}:{})".format(device.hostname, device.ips[0], device.port))
            try:
                self.dbus_service.start_stream(device, res[0], res[1], False)
            except Exception as e:
                MhErrorDialogs(self).stream_error(e)
        else:
            self.dbus_service.stop_stream(device.ips[0], device.port)