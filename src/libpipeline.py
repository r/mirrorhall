'''
Generates hardware-specific GStreamer video pipelines for optimized streaming
'''

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib, Gio, GObject


class PipelineService:
    # Hardware compatible with the available sources, sorted by preference
    known_decoders = [
        {   # Generic libav - software-based but pretty fast and reliable
            'name': 'avdec_h264',
            'args': '',
            'h264parse': False,
            'software': True,
        },
        {   # Intel / AMD using VAAPI - new plugin
            'name': 'vaapih264dec',
            'args': '',
            'h264parse': False,
            'software': False,
        },
        {   # Intel / AMD using VAAPI - bad plugin
            'name': 'vah264dec',
            'args': '',
            'h264parse': True,
            'software': False,
        },
        {   # Qualcomm / Venus
            'name': 'v4l2h264dec',
            'args': '',
            'h264parse': True, # TODO: check this
            'software': False,
        },
        {   # NVidia devices
            'name': 'omxh264dec',
            'args': '',
            'h264parse': False, # TODO: check this
            'software': False,
        },
        {   # Other weird NVidia devices
            'name': 'nvv4l2decoder',
            'args': '',
            'h264parse': False, # TODO: check this
            'software': False,
        },
        {   # OpenH264 - software-based - looks very bad
            'name': 'openh264dec',
            'args': '',
            'h264parse': True,
            'software': True,
        },
    ]
    gsettings = Gio.Settings.new("eu.nokun.MirrorHall")
    
    def __init__(self) -> None:
        Gst.init(None)
        GLib.threads_init()

    # Returns a GStreamer sink pipeline description for the given port
    def create_sink_pipeline_description(self, port) -> str:
        # TODO: fallback pipeline using switchbin?
        source, options, caps = self.find_source(port)
        return '%s %s ! %s ! queue ! %s' % (source, options, caps, self.find_decoder())
            

    # Builds a GStreamer sink pipeline object on a given port
    def create_sink_pipeline(self, port):
        pipeline_desc = self.create_sink_pipeline_description(port)

        if self.gsettings.get_string('force-decode-pipeline') != '':
            print("Forcing decode pipeline:\t%s" % self.gsettings.get_string('force-decode-pipeline'))
            print("instead of generated pipeline:\t%s" % pipeline_desc)
            pipeline_desc = self.gsettings.get_string('force-decode-pipeline')


        print("Sink pipeline: " + pipeline_desc)
        return Gst.parse_bin_from_description(pipeline_desc, True)

    # Picks the best available source for the incoming stream
    def find_source(self, port):
        # GStreamer sources supporting this stream, sorted by preference
        # [ source, options, caps/decoder ]
        known_sources = [
            # mirrorhall-h264-udp
            (
                'udpsrc',
                'port=%d' % port, 
                'application/x-rtp, clock-rate=90000, media=video, encoding-name=H264, payload=96, stream-format=byte-stream ! queue ! rtph264depay'
            ),
            # TODO: RTSP support (requires server port knowledge)
            # (
            #     'rtspsrc', 
            #     'location="rtsp://localhost:%d/live" latency=0 buffer-mode=auto'%(port),
            #     'parsebin ! queue ! decodebin '
            # ),
        ]
        for source, options, caps in known_sources:
            if Gst.ElementFactory.find(source):
                return (source, options, caps)
        
        raise Exception('No compatible source has been found. Is GStreamer fully installed?')

    # Picks the best available decoder
    def find_decoder(self):
        if self.gsettings.get_string('force-decode-pipeline'):
            print("Forcing decode pipeline: %s" % self.gsettings.get_string('force-decode-pipeline'))
            return self.gsettings.get_string('force-decode-pipeline')

        for decoder in self.known_decoders:
            if Gst.ElementFactory.find(decoder['name']):
                if self.gsettings.get_boolean('force-encode-software') and not decoder['software']:
                    print("Skipping decoder %s because software-based decoding is forced" % decoder['name'])
                print("Best decoder: %s %s" % (decoder['name'], decoder['args']))
                parser = 'h264parse ! ' if decoder['h264parse'] else ''
                return ('%s%s %s  ! queue ! videoconvert' % (parser, decoder['name'], decoder['args']))
        
        raise Exception("No suitable decoder installed. Some additional GStreamer plug-in might be needed")

    # Picks an available video sink for the GTK 4 window
    def find_sink(self):
        use_clapper = self.gsettings.get_boolean('use-clappersink')
        sinks = ['gtk4paintablesink', 'clappersink']
        if use_clapper:
            sinks.reverse()
        
        for sink in sinks:
            if Gst.ElementFactory.find(sink):
                print("Using sink: %s" % sink)
                return sink
        
        raise Exception("No suitable video sink found. Please install one of the following:\n%s" % sinks)


    def create_sender_pipeline_description(self, node_id, format_element, host, port, encoder, options):
        # sink for mirrorhall-h264-udp streams
        stream_sink = 'udpsink host=%s port=%d'%(host, port)
            
        return 'pipewiresrc path=%u ! %s videoconvert ! queue ! %s %s ! video/x-h264, stream-format=byte-stream ! queue ! rtph264pay ! %s'%(
                node_id, format_element, encoder, options, stream_sink)


    # Creates a GStreamer sender pipeline for the given PipeWire object, host, and port
    def create_sender_pipeline(self, node_id, format_element, host, port):
        bitrate = self.gsettings.get_int('bitrate')
        x264_preset = self.gsettings.get_string('x264-preset')
        x264_tune = 'tune=zerolatency' if self.gsettings.get_boolean('x264-zerolatency') else ''

        # should be sorted by preference
        encoders = [
            # TODO: spicy encoder -> ["x264enc", "%s bitrate=%d speed-preset=%s byte-stream=true"%(x264_tune, bitrate,x264_preset)],
            ["vah264enc", "target-usage=7 bitrate=%d"%bitrate],
            ["vaapih264enc", "bitrate=%d"%bitrate],
            ["avenc_h264_omx","bitrate=%d"%bitrate],
            ["v4l2h264enc",""],
            ["openh264enc","complexity=low bitrate=%d"%bitrate],
        ]


        if self.gsettings.get_string("force-encode-pipeline") != "":
            print("Forcing pipeline: %s"%self.gsettings.get_string("force-encode-pipeline"))
            return Gst.parse_launch(self.gsettings.get_string("force-encode-pipeline"))

        if self.gsettings.get_boolean("force-encode-software"):
            print("Forcing software encoder. This might be slow.")
            encoders = [
                ["openh264enc","bitrate=%d"%bitrate],
            ]

        for (encoder, options) in encoders:
            try:
                if Gst.ElementFactory.find(encoder) is None:
                    raise Exception("Encoder not found")

                pipeline_desc = self.create_sender_pipeline_description(node_id, format_element, host, port, encoder, options)
                l = Gst.parse_launch(pipeline_desc)
                if l:
                    print("Stream pipeline: %s"%pipeline_desc)
                    return l
            except Exception:
                continue

        raise Exception("No suitable encoder found")
    
    def start_rtsp_server(self, node_id, endpoint = 'mirror', bitrate = 6500):
        try:
            gi.require_version('GstRtspServer', '1.0')
            from gi.repository import GstRtspServer

            class RtspFactory(GstRtspServer.RTSPMediaFactory):
                def __init__(self, node_id):
                    GstRtspServer.RTSPMediaFactory.__init__(self)
                    self.node_id = node_id

                def do_create_element(self, url):
                    s_src = "pipewiresrc path=%s ! video/x-raw,rate=30,width=1280,height=720 ! videoconvert "%(self.node_id)
                    s_openh264 = "openh264enc complexity=low bitrate=%d"%(bitrate)
                    pipeline_str = "( {s_src} ! queue ! {s_openh264} ! queue ! rtph264pay name=pay0 pt=96 )".format(**locals())
                    print(pipeline_str)
                    return Gst.parse_launch(pipeline_str)
        except:
            raise Exception("gst-rtsp-server is not installed")
        
        self.server = GstRtspServer.RTSPServer()
        f = RtspFactory(node_id)
        f.set_shared(True)
        m = self.server.get_mount_points()
        m.add_factory("/"+endpoint, f)
        self.server.attach(None)
