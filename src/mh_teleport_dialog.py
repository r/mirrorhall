import gi, socket
from gi.repository import Gtk

from .libcast import MhNetworkDeviceData

class MhTeleportDialog(Gtk.Dialog):
    host_entry = Gtk.Entry()
    port_entry = Gtk.Entry()

    def __init__(self, parent_window, on_connect):
        self.parent_window = parent_window
        super().__init__(title="Teleport", transient_for=parent_window)
        self.set_modal(True)
        self.set_default_response(Gtk.ResponseType.OK)
        self.set_resizable(False)
        header_bar = Gtk.HeaderBar()
        header_bar.set_show_title_buttons(False)


        content_area = self.get_content_area()
        container_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20)
        container_box.set_margin_top(20)
        container_box.set_margin_bottom(20)
        container_box.set_margin_start(20)
        container_box.set_margin_end(20)
        label = Gtk.Label.new("Make sure the mirror is reachable (e.g. on UDP)")
        container_box.append(label)
        self.host_entry.set_placeholder_text("IP address")
        self.host_entry.set_hexpand(True)
        self.port_entry.set_placeholder_text("port")
        self.port_entry.set_input_purpose(Gtk.InputPurpose.NUMBER)
        self.port_entry.set_hexpand(False)
        self.port_entry.set_max_width_chars(5)
        entry_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        entry_box.add_css_class("linked")
        entry_box.append(self.host_entry)
        entry_box.append(self.port_entry)
        container_box.append(entry_box)
        content_area.append(container_box)


        connect_button = Gtk.Button.new_with_label("Connect")
        connect_button.add_css_class("suggested-action")
        cancel_button = Gtk.Button.new_with_label("Cancel")
        connect_button.connect("clicked", on_connect)
        cancel_button.connect("clicked", lambda button: self.close())
        header_bar.pack_start(cancel_button)
        header_bar.pack_end(connect_button)

        self.set_titlebar(header_bar)